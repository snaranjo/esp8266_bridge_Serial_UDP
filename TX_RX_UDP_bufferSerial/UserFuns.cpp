#include "UserTypes.h"

void checkUDP(UDP_obj *p, Stream *sp)
{

  int packetSize = p->u.parsePacket();

  if (packetSize)
  {
    char inPacket[255]; // buffer for incoming packets
    int len = p->u.read(inPacket, 255);

    if (len > 0)
      inPacket[len] = 0;
    if (len == 2 && (inPacket[0] == inPacket[1]))
      singleCharDecision(sp, p, inPacket[0]);
    else
    {
      sp->printf("%s\n", inPacket);
    } //else if packet is bigger than one char
  }   // if packetSize
} // checkReplyUDP

void udptx_remoteInfo(UDP_obj *p)
{ //char * remoteAdd, uint16_t remoteP) {

  char replyMsg[30];
  int numFormat = sprintf(replyMsg, "New remote Add: %s Port: %d",
                          p->remAdd, p->remPort);
  if (numFormat > 0)
  {
    //    Serial.write(replyMsg,numFormat);
    //    Serial.println();
    p->u.beginPacket(p->remAdd, p->remPort);
    int bytesSent = p->u.write(replyMsg, numFormat);
    p->u.endPacket();

    if (bytesSent > 0)
      p->knownInfo = true;
    else
      p->knownInfo = false;
  } //numFormat > 0
} //int udptx_remotInfo

void sendUDPmsg(Stream *sp, UDP_obj *p, uint8_t *buffer, int size)
{
  // sp->println();
  // sp->print("Attempt send UDP msg: ");
  // sp->write(buffer, size);
  // sp->println();

  int beginStatus = p->u.beginPacket(p->remAdd, p->remPort);

  if (beginStatus)
  {
    int numBytesSent = p->u.write(buffer, size);
    //should delay status of endPacket since it the other computer cannot read fast enough
    p->u.endPacket();
    //    if(numBytesSent > 0) {
    //      Serial << "Wrote " << numBytesSent << " bytes.";
    //      } //unsuccessful TX

  } // if begin status is good
  else
    sp->println("XX");
} // void sendUDPmsg

uint32_t ptr2Num(Stream *sp, char *charNum, char stopChar)
{
  //ptr2NUm expects charNum to point to set of numbers in char format
  //   The pointer will backtract adding up numbers until
  //   it reaches stopChar and returns the result
  uint16_t i = 0;
  uint32_t calcNum = 0;
  //  sp->print("ptr2Num fun: ");
  //  sp->println(charNum);
  //  char *msg_ptr = &rxPacket[rxSize - 1];
  //Assume a 10 MB cap for a given file. To exit this loop due to error.
  while (*--charNum != stopChar)
  {
    //  sp->write(' ');
    //  sp->print(*charNum);
    calcNum += (*charNum - '0') * pow(10, i++);
    //  sp->write('\t');
    //  sp->println(calcNum);
  }
  //  Serial.printf("\nRX %d \t",rxdNum);
  if (i == 8)
    return 65535; //Indicate an error with number
  else
    return calcNum;
} //end ptr2Num

// uint32_t parseNum( char *strNum, )
uint32_t awaitPrevTXnum(UDP_obj *p, Stream *sp)
{
  //Await packet, expecting format <NUM> and return NUM
  int packetSize = 20;
  char rxPacket[packetSize];
  uint32_t rxdNum = 0;

  unsigned long t = millis();
  while (!p->u.parsePacket())
  {
    if (millis() - t > 5000)
    { //Timeout
      // sp->print('XX');
      return 65525;
      // t = millis();
      //      Serial.write('.');
    } //if less that 250ms
  }

  int rxSize = p->u.read(rxPacket, packetSize);
  rxPacket[rxSize] = 0;

  //Look through packet to find packet size w/ format <NUM>
  if (rxPacket[rxSize - 1] == '>' && rxPacket[0] == '<')
  {
    return ptr2Num(sp, &rxPacket[rxSize - 1], '<');
  } //indicate that we found a properly formatted msg
  else
    return 65535; //Instead of 0 since 0 could be sent

} //void awaitPrevTXnum
void sendVerifyUDPmsg(Stream *sp, UDP_obj *p, uint8_t *buffer, int size)
{
  uint16_t msg_limit = 512;
  uint16_t numBytesSent = 0;
  uint16_t replySize = 25;
  char replyPacket[replySize]; //buffer for incoming reply <NUM>

  uint8_t *msg_ptr = buffer;

  while (numBytesSent < size)
  {

    if (p->u.beginPacket(p->remAdd, p->remPort))
    { //Setup packet and attempt to send
      numBytesSent = p->u.write(msg_ptr, size);
      //should delay status of endPacket since it the other computer cannot read fast enough
      p->u.endPacket(); //This is what actually sends the msg
      if (numBytesSent > 0)
      { //bytes were sent, expect a reply with the same number of bytes
        while (!p->u.parsePacket())
          ;

        int len = p->u.read(replyPacket, replySize);
        replyPacket[len] = 0;
        // sp->print("Rx msg, going to parse: ");
        // sp->println(replyPacket);
        uint32_t rxNum = ptr2Num(sp, &replyPacket[len - 1], '<');
        // sp->print("Confirm receipt: ");
        // sp->print(rxNum); //Use for debugging...
      } //successful TX
    }   // if begin status is good

  } //while bytes being sent
} // void sendFragUDPmsg

void sendFragUDPmsg(UDP_obj *p, uint8_t *buffer, int size)
{
  uint16_t msg_limit = 512;
  bool fragMSG = false;
  uint16_t totalBytesSent = 0;
  uint16_t msg_size, msg_rem, numBytesSent;

  if (size > msg_limit)
  {
    fragMSG = true;
    msg_size = msg_limit;
    msg_rem = size - msg_limit;
  } //if message is too big, cut down into fragments
  else
  {
    msg_size = size;
    msg_rem = 0;
  }

  uint8_t *msg_ptr = buffer;

  while (totalBytesSent < size)
  {

    if (p->u.beginPacket(p->remAdd, p->remPort))
    { //Setup packet and attempt to send
      numBytesSent = p->u.write(msg_ptr, msg_size);
      //should delay status of endPacket since it the other computer cannot read fast enough
      p->u.endPacket(); //This is what actually sends the msg
      if (numBytesSent > 0)
      {
        //        Serial.printf(">%d", numBytesSent);
      } //unsuccessful TX
    }   // if begin status is good
    // else
    //   Serial.printf("/nCould not begin packet\n\n");

    totalBytesSent += numBytesSent;

    if (fragMSG)
    {
      //      Serial.printf("..%d bytes left to TX.",msg_p->rem);
      //UNCOMENT BELOW when awaitPrevTXnum fixed
      //   if ( (uint16_t)awaitPrevTXnum(p) == msg_size )
      //   {
      if (totalBytesSent == size)
      {
        fragMSG = false;
        //          Serial.printf("TX total of %d/%d bytes.\n",totalBytesSent,size);
        return;
      }
      //        else Serial.printf(".ready to TX more.\n");
      //   }

      msg_ptr = &buffer[totalBytesSent];
      if (msg_rem > msg_limit)
      {
        msg_size = msg_limit;
        msg_rem -= msg_limit;
      }
      else
      {
        msg_size = msg_rem;
        msg_rem = 0;
        //        fragMSG = false;
      } //if-else msg_rem > msg_limit
    }   //if fragMSG
        //    Serial.printf("\n\ttotalBytes %d \t size %d",totalBytesSent,size);
  }     //while bytes being sent
} // void sendFragUDPmsg

int collectSerialPacket(Stream *sp, char *charArr, uint16_t arrSize)
{
  // sp->print("collSP ");
  int i = 0;
  unsigned long t = millis();
  // while (sp->available() || (millis()-t) < 2) {
  //   charArr[i++] = (char)sp->read();
  //   t = millis();
  //   // sp->write(charArr[i-1]);
  //   // sp->write('_');
  //   // delay(3); //Seems fine at 2 & 5 ms
  // }
  // sp->println((millis()-t));
  while (millis() - t < 3)
  {
    if (sp->available())
    {
      charArr[i++] = (char)sp->read();
      t = millis();
    }
  } //end while
  return i;
}

void checkSerial(Stream *sp, UDP_obj *p)
{

  if (sp->available())
  {
    int packetSize = 255;
    char serialPacket[packetSize];
    int ind = collectSerialPacket(sp, serialPacket, packetSize);

    if (ind > 0 && p->knownInfo)
    {
      if (ind == 1)
      {
        // sp->println("serial singleChardec");
        singleCharDecision(sp, p, serialPacket[0]);
      }
      else
        sendUDPmsg(sp, p, (uint8_t *)serialPacket, ind);
    } // ind> 0
  }   //if serial avialable
} //end void checkSerial

void singleCharDecision(Stream *sp, UDP_obj *p, char decision)
{
  //simple fun to do special things when a single char is received.
  //assuming it was sent for a particular task
  // char decision = *input;
  // Serial.printf("\nReceived %c as a decision...\n",decision);
  switch (decision)
  {

  case 'v': //send 16bit ints in 512 byte packets
  {

    int arraySize = 1200;
    uint16_t maxint = 0;
    uint16_t integers[arraySize];
    for (uint16_t i = 0; i < arraySize; i++)
    {
      integers[i] = i;
      if (integers[i] > maxint)
        maxint = integers[i];
    }
    unsigned long t = micros();
    sendFragUDPmsg(p, (uint8_t *)integers, arraySize * 2);
    t = micros() - t;
    sp->printf("\nTX time %u usec, max value %u\n",
               t, maxint);
    break;
  }
  case 'm': //about the max sized bytes to send before data gets lost in TX
  {

    int arraySize = 800;
    uint16_t maxint = 0;
    uint16_t integers[arraySize];
    for (uint16_t i = 0; i < arraySize; i++)
    {
      integers[i] = i;
      if (integers[i] > maxint)
        maxint = integers[i];
    }
    unsigned long t = micros();
    sendUDPmsg(sp, p, (uint8_t *)integers, arraySize * 2);
    t = micros() - t;
    sp->printf("\nTX time %u usec, max value %u\n",
               t, maxint);
    break;
  }
  case 'n': //Redefine new remoteAdd and Port,  confirm with a 'y' reply?
  {
    p->u.remoteIP().toString().toCharArray(p->remAdd, 14);
    p->remPort = p->u.remotePort();
    udptx_remoteInfo(p);
    break;
  }
  case 'p': //provide IP info
  {
    char broadcastMSG[50];
    int numBroadcast = sprintf(broadcastMSG, "Listening at IP %s, port %d", p->localIP, p->localPort);

    //    Serial.write(broadcastMSG, numBroadcast);
    sendUDPmsg(sp, p, (uint8_t *)broadcastMSG, numBroadcast);
    break;
  }
  case 'x': //send 3 16bit ints
  {
    int16_t fixInt[3] = {3, 4, 5};
    sendUDPmsg(sp, p, (uint8_t *)fixInt, 3 * 2);
    break;
  }

  case 'g':
  { //same as v but with float values. to confirm consistency
    uint16_t maxBytes = 2500;
    uint16_t sizeArr = maxBytes / 4;
    float rads[sizeArr];
    float pi = 3.1416;
    float denom[] = {8, 16, 32, 64};
    float value;
    float last_value = -pi / 2;
    for (int i = 0; i < sizeArr; i++)
    {
      if (i < round(sizeArr / 4))
        value = 1 / denom[0];
      else if (i > round(sizeArr / 4) && i < round(sizeArr / 2))
        value = 1 / denom[1];
      else if (i > round(sizeArr / 2) && i < round(3 * sizeArr / 4))
        value = 1 / denom[2];
      else
        value = 1 / denom[3];
      rads[i] = last_value + value;
      last_value = rads[i];
    } //for loop

    char bytesNumMsg[50];
    int bytesMsgSize = sprintf(bytesNumMsg, "<m\\%u><%s>",
                               maxBytes, "GenericSineData");
    sendFragUDPmsg(p, (uint8_t *)bytesNumMsg, bytesMsgSize);
    // awaitPrevTXnum( p); //uncomment when you fix this fun
    unsigned long t = millis();
    sendUDPmsg(sp, p, (uint8_t *)rads, maxBytes);
    t = millis() - t;
    sp->printf("\nTX time %u msec\n", t);
    break;
  }
  case 'l':
  {
    char ml_msg[50];
    int ml_size = sprintf(ml_msg, "This \n is a \n multi-line \n msg");
    //Send multi-line message to test receving it in GUI
    sendUDPmsg(sp, p, (uint8_t *)ml_msg, ml_size);

    break;
  }
  case '~':
  {
    largeDataTransfer(sp, p);
    break;
  }
  } // end switch
} // void singleCharDecision

void largeDataTransfer(Stream *sp, UDP_obj *p)
{
  //Prepare to download large message from serial and pass data through UDP in 512 byte packets.
  //Confirm that no bytes are lost between Serial AND UDP comms.
  uint16_t buffSize = 512; //UDP 512 byte tx seems effective.
  uint8_t u_buffer[buffSize];
  unsigned long t;
  unsigned long t_delay = 5000;
  char clrMsg[] = "<XX>";

  //Need to retrieve file size info, query DUE
  char startChar = '~';
  byte numChar = sp->write(startChar);
  if (numChar > 0)
  {
    //Assume that a sent char confirms comms. with DUE
    //Let MATLAB know
    // sp->println("Sending UDP");
    sendUDPmsg(sp, p, (uint8_t *)&startChar, 1);
  }
  uint16_t numRequest = awaitPrevTXnum(p, sp); //Wait for input from MATLAB for which file to read
  if (numRequest >= 65525)
  { //Indicate timeout or incorrect format
    char badFormat[] = "<E><IncorrectNumFormat>";
    sp->write(clrMsg, 4);
    sendUDPmsg(sp, p, (uint8_t *)badFormat, sizeof(badFormat) - 1);
    return;
  }
  else
  {
    char numStr[15];
    int numStrLen = sprintf(numStr, "<%lu>", numRequest);
    numStr[numStrLen] = 0;
    sp->write(numStr, numStrLen); //Send numRequest
  }

  uint16_t msgMax = 255;
  char fileMsg[msgMax];

  //DUE should be polling up the file and will send <m/fileSize><fileName>
  t = millis();
  while (!sp->available())
  {
    if (millis() - t > t_delay)
    {
      sp->write(clrMsg, 4);
      char noMsg[] = "<E><noRxFile>";
      sendUDPmsg(sp, p, (uint8_t *)noMsg, sizeof(noMsg) - 1);
      return;
    }
  }

  // sp->println("Serial available...reading msg..");
  int msgSize = collectSerialPacket(sp, fileMsg, msgMax);
  fileMsg[msgSize] = 0;
  // sp->print("Obtained msg: ");
  // sp->println(fileMsg);
  // sp->write(fileMsg, msgSize);
  // sp->println();

  //Send fileMsg to MATLAB
  // sp->println("Send msg to UDP");
  sendUDPmsg(sp, p, (uint8_t *)fileMsg, msgSize);
  // sp->println("Sent UDP msg!");

  //Determine fileSize
  uint16_t i = 0;
  t = millis();
  while (i < msgSize)
  {
    if (fileMsg[i++] == '>' && fileMsg[i] == '<')
      break;
    if (millis() - t > t_delay)
    {
      sp->write(clrMsg, 4);
      char noFormatMsg[25];
      int nfmSize = sprintf(noFormatMsg, "<E><Format not found>");
      sendUDPmsg(sp, p, (uint8_t *)noFormatMsg, nfmSize);
    }
  }
  // sp->println();
  // sp->println("Found the >< : ");
  // sp->println(i);
  uint32_t fileSize = ptr2Num(sp, &fileMsg[i - 1], '/');
  // sp->print("Found file size: ");
  // sp->println(fileSize);
  // sp->println("Comparing fileSize to number from MATLAB..");
  if (fileSize == awaitPrevTXnum(p, sp))
  { //MATLAB returns the expected size
    //Notify DUE of fileSize
    char fileSizemsg[20];
    uint16_t msgLength = sprintf(fileSizemsg, "<%lu>", fileSize);
    //Seems to get here but doesn't proceed...why??!----*!*!*!!*!*!
    // sp->println("Received filesize from matlab: ");
    sp->printf(fileSizemsg, msgLength); //("<%u>", fileSize);
    // sendUDPmsg(sp, p, (uint8_t *)fileSizemsg, msgLength); //<--could this be the extra 7 bytes?
    uint16_t cnt = 0;
    // return;
    // sp->println("R2S cnt<fileSize");

    while (cnt < fileSize)
    { //run loop until all bytes have been transferred
      uint16_t bytesRx = 0;
      uint16_t bytesInMsg = 0;
      uint8_t replyMax = 20;
      char replyMsg[replyMax];
      sp->write('R');
      for (uint16_t i = 0; i < 4; i++)
      { //RX 512 byte block in 4 - 128 byte packets
        t = millis();
        while (!sp->available())
        { //Wait for serial
          if (millis() - t > t_delay)
          {
            char errorMsg[20];
            int eSize = sprintf(errorMsg,
                                "<E><%lu/%lu>", bytesInMsg, fileSize);
            sendUDPmsg(sp, p, (uint8_t *)errorMsg, eSize);
            sp->write(clrMsg, 4);
            return;
          } //if major delay tripped
        }
        bytesRx = collectSerialPacket(sp, (char *)u_buffer + (i * 128), 128);
        bytesInMsg += bytesRx;
        int replySize = sprintf(replyMsg, "<%u>", bytesRx);
        sp->print(replyMsg);

      } // for loop
      if (bytesInMsg == 512)
      {
        sendVerifyUDPmsg(sp, p, u_buffer, buffSize);
        cnt += bytesInMsg;
      }
    } // while cnt < fileSize
  }   //fileSize matches what MATLAB expects
  else
  { //fileSize did not match MATLAB
    char falseMsg[] = "<E><fileSize mismatch>";
    int fmSize = sizeof(falseMsg) - 1;
    sp->write(clrMsg, 4);
    sendUDPmsg(sp, p, (uint8_t *)falseMsg, fmSize);
  }

  //if majorDownload, what's the status of the packet size?
  //Confirm size with MATLAB??
  //Get fileSize

  //Send msg to MATLAB
  //Await confirmation from MATLAB
  //commence majorDownload

} // void largeDataTranser