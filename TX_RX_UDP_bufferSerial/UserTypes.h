#ifndef UserTypes_h
#define UserTypes_h
#include "Arduino.h"
#include <WiFiUdp.h>

//*--*--*--*--*--*--*--*--*--Variable Definitions*--*--*--*--

struct UDP_obj {
  WiFiUDP u;
  bool knownInfo = false;//true;
  bool dwnldProgress = false;
  char remAdd [14];// = "192.168.1.112";
  char localIP[14];
  uint16_t remPort;// = 50280;
  uint16_t localPort = 5420;
};

//*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--*--

//Fun Definitions
//UDP Funs
void     checkUDP        (UDP_obj * p, Stream * sp);
void     udptx_remoteInfo(UDP_obj * p);
void     sendUDPmsg      (Stream * sp, UDP_obj * p, uint8_t *buffer, int size);
uint32_t ptr2Num         (Stream * sp, char * charNum, char stopChar);
uint32_t awaitPrevTXnum  (UDP_obj * p, Stream * sp);
void     sendVerifyUDPmsg(Stream * sp, UDP_obj * p, uint8_t *buffer, int size);
void     sendFragUDPmsg(UDP_obj * p, uint8_t *buffer, int size);
// Serial Funs
int     collectSerialPacket(Stream * sp, char * charArr, uint16_t arrSize);
void    checkSerial        (Stream * sp, UDP_obj * up);
void    singleCharDecision (Stream * sp, UDP_obj * up, char decision);
void    largeDataTransfer  (Stream * sp, UDP_obj * p);

// void userSetup();
#endif  // UserTypes_h
