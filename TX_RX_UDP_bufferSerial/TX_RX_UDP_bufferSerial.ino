#include <ESP8266WiFi.h>
//#include <IPAddress.h>
//#include <WiFiUdp.h>
// #include <Streaming.h>
#include "UserTypes.h"
//Program is able to response to a UDP TX with a prewritten message. 
//It can also send data received from SERIAL throuh UDP.

// #define UDP_TX_PACKET_MAX_SIZE 4000

// const char* ssid      = "robot";
// const char* password  = "4rlr0b0t";
const char* ssid     = "vicon1"; 
const char* password = "vicon1vtd"; 
// const char* ssid     = "FEED-MI";
// const char* password = "412NewH@!!West";
//const char* ssid     = "OrangeMBPn";


IPAddress ip_add(192,168,1,158); //192.168.1.158 robot //192.168.0.19 FEED-MI
IPAddress gate(192, 168, 1, 1); 
IPAddress subnet(255,255,255,0);
 

UDP_obj u_o;
// WiFiUDP Udp;

// char remAdd[14];// = "192.168.1.112";
// uint16_t remPort;// = 50280;
// bool knownRemotInfo = false;//true;
// bool majorDownload  = false;


// const unsigned int localUdpPort = 5420; //4210;  // local port to listen on
//char  replyPacket[] = "received UDP message";  // a reply string to send back
//char  serialFixMsg[] = "RX Serial";


void setup()
{
  Serial.begin(115200);

//  Serial.printf("\nConnecting to %s ", ssid);
  
  WiFi.begin(ssid, password);
  WiFi.config(ip_add, gate, subnet); //this method is not found in the esp8266 lib on pc
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
  //  Serial.print(".");
  }
//  Serial.println(" connected");


  u_o.u.begin(u_o.localPort);
  WiFi.localIP().toString().toCharArray(u_o.localIP, 14);

  char broadcastMSG[50];
  int numBroadcast = sprintf(broadcastMSG, 
  "Now listening at IP %s, UDP port %d\n", u_o.localIP, u_o.localPort);
  
//  Serial.write((uint8_t*)broadcastMSG, numBroadcast);
  
//  Udp.beginPacketMulticast( gate,1000, ip_add);
//  Udp.write(broadcastMSG, numBroadcast);
//  Udp.endPacket();
}


void loop()
{
  checkUDP(&u_o, &Serial);
  checkSerial(&Serial, &u_o);
}
