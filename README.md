ESP8266 communicates over serial with the DUE and communicates with Matlab 
over UDP. 

There a few special characters reserved for special cases such as:
"nn" for new port and IP address used for connecting to it and signaling that 
     you are an interested client. 
"pp" ping to see the IP and port of the ESP8266, this is fixed and can be used 
     to check that the ESP8266 is functional.
 
 The network settings for this dongle are currently hard coded as such:
    ssid     = "vicon1" 
    ip_add(192, 168,   1, 158);
      gate(192, 168,   1, 1); 
    subnet(255, 255, 255, 0);
In order to connect to it, your IPv4 network settings should be connected to
vicon1 and have the same subnet and gateway address
and a IP address such as 192.168.1.X.

In the current state, the dongle reaches an overload state when transferring data
over 100 KB within a file transfer session. 

## Notes for Programming 
The ESP8266 can be programmed with the standard Arduino IDE using the 
ESP8266WiFi.h library 
https://github.com/esp8266/Arduino/blob/master/libraries/ESP8266WiFi/src/ESP8266WiFi.h
In order to program you will need a FTDI programming chip to interface with a 
serial port.
The TX/RX lines on the interface board are improperly labeled so when connected 
to any other device, they should be paired directly as such 
ESP TX - dev_0 TX       instead of the traditional   ESP TX - dev_0 RX
ESP RX - dev_0 RX                                    ESP RX - dev_0 TX

When ready to upload, wait until the IDE says it's "Uploading.." 
then HOLD DOWN the PROG button while pressing the RESET button once. 
This puts the dongle in program mode. You can let go of the PROG button after
release the RESET button. The interface board connected to the ESP8266 was 
designed by James Dotterwich and Wosen Wolde so talk with them if you need help
with the board or have quesiton of the power supply curcuit. 
